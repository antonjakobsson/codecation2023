class_name Hurtbox
extends Area2D

func _init():
	collision_layer = 0
	collision_mask = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("area_entered",_on_area_entered)
	connect("body_entered",_on_body_entered)
	connect("body_exited", _on_body_exited)
	connect("area_exited", _on_area_exited)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_area_entered(area):
	if area is Hitbox:
		if owner.has_method("take_damage"):
			owner.take_damage(area.damage)
	if area is Gravity:
		if owner.has_method("pull_toward"):
			owner.pull_toward(true, area.global_position)

func _on_area_exited(area):
	if area is Lava:
		if owner.has_method("update_lava_state"):
			owner.update_lava_state(false)
	if area is Gravity:
		if owner.has_method("pull_toward"):
			owner.pull_toward(false, area.global_position)

func _on_body_entered(area):
	if area is Lava:
		if owner.has_method("update_lava_state"):
			owner.update_lava_state(true)

func _on_body_exited(area):
	if area is Lava:
		if owner.has_method("update_lava_state"):
			owner.update_lava_state(false)
