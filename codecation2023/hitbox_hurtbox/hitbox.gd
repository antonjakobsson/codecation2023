class_name Hitbox
extends Area2D

@export var damage = 15

func _init():
	collision_layer = 2
	collision_mask = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
