extends Control

#Actual entry point of game - main

var address
@export var PORT = "8000"
var peer
var scene = load("res://main.tscn")
var scene_object = null
@export var scoreboard = {}
var namecounter = {} # To differentiate duplicate names

# Called when the node enters the scene tree for the first time.
func _ready():
	multiplayer.peer_connected.connect(peer_connected)
	multiplayer.peer_disconnected.connect(peer_disconnected)
	multiplayer.connected_to_server.connect(connected_to_server)
	multiplayer.connection_failed.connect(connection_failed)
	$WinnerButton.hide()
	$StartGameButton.hide()
	$WinnerButton/Timer.connect("timeout", _on_timer_timeout)

	
func _on_timer_timeout():
	$WinnerButton.hide()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if scene_object != null:
		if scene_object.playersalive <= 1:
			# Show last player name
			var winner = scene_object.PlayerObjects[0].playername
			
			# Update score
			for i in GameManager.Players:
				if GameManager.Players[i].name == winner:
					GameManager.Players[i].score += 1

			# Restart game
			RestartGame.rpc(winner)

# This gets called on the server and the clients 
func peer_connected(id):
	print("Player connected: " + str(id))


func peer_disconnected(id):
	print("Player disconnected: " + str(id))


# Only called on client side
func connected_to_server():
	print("Successfully connected to server")
	#Call using RPC_ID to only make server run this code
	SendPlayerInformation.rpc_id(1, $UserNameLabel/UserName.text, multiplayer.get_unique_id())


# Only called on client side
func connection_failed():
	print("Connection failed")


@rpc("any_peer")
func SendPlayerInformation(name, id):
	if !GameManager.Players.has(id):
		for i in GameManager.Players:
			if GameManager.Players[i].name == name:
				if name not in namecounter:
					namecounter[name] = 0
				namecounter[name] += 1
				name += "("+str(namecounter[name])+")"
		GameManager.Players[id] = {
			"name": name,
			"id": id,
			"score": 0,
			"mana": 100,
			"health": 100,
		}
		print("SendPlayerInformation: Added player " + str(name) + " "+ str(id))

	if multiplayer.is_server():
		for i in GameManager.Players:
			SendPlayerInformation.rpc(GameManager.Players[i].name, i)
	UpdateLobbyInfo()
	
@rpc("any_peer", "call_local")
func UpdateLobbyInfo():
	#Use GameManager object
	$LobbyLabel.text = "Lobby: " + str(len(GameManager.Players)) + " / 16 Players"
	var playernamestr = ""
	for i in GameManager.Players:
		playernamestr += str(GameManager.Players[i].name) + "\n"
	$LobbyLabel/LobbyInfo.text = playernamestr
		

@rpc("any_peer", "call_local")
func StartGame():
	# Instantiate world
	#if scene_object != null:
	$StartGameButton.show()
	$StartGameButton.text = "Starting Game\n 3"
	$StartGameButton/Timer.connect("timeout", timer_1)
	$StartGameButton/Timer.start(1)

#@rpc("any_peer")
func timer_1():
	$StartGameButton.text = "Starting Game\n 2"
	$StartGameButton/Timer1.connect("timeout", timer_2)
	$StartGameButton/Timer1.start(1)

#@rpc("any_peer")
func timer_2():
	$StartGameButton.text = "Starting Game\n 1"
	$StartGameButton/Timer2.connect("timeout", StartGameFunction)
	$StartGameButton/Timer2.start(1)

#@rpc("any_peer", "call_local")
func StartGameFunction():
	$StartGameButton.hide()
	scene_object = scene.instantiate()
	get_tree().root.add_child(scene_object)
	self.hide()

@rpc("any_peer", "call_local")
func RestartGame(winner):
	print("Called restartgame")
	if scene_object != null:
		scene_object.queue_free()
		
	# Update winner text
	var winnerstr = "WINNER\n" + str(winner)
	$WinnerButton.text = winnerstr
	$WinnerButton.show()
	$WinnerButton/Timer.start(3)

	# Update scoreboard
	update_scoreboard.rpc()
	self.show()

@rpc("any_peer", "call_local")
func update_scoreboard():
	var scoreboardstr = ""
	for i in GameManager.Players:
		if GameManager.Players[i].score > 0:
			scoreboardstr += str(GameManager.Players[i].name) + ": " +  str(GameManager.Players[i].score) + "\n"
	$Scoreboard.text = scoreboardstr

func _on_host_button_button_down():
	peer = ENetMultiplayerPeer.new()
	var error = peer.create_server(int(PORT), 16) # 16 max players
	if error != OK:
		print("Cannot host: " + str(error))
		return
	peer.get_host().compress(ENetConnection.COMPRESS_RANGE_CODER) #Can use other compression schemes
	
	multiplayer.set_multiplayer_peer(peer)
	print("Waiting for players...")
	$HostButton/HostingStatus.text = "HOSTING ON " + $MyIPAddress.local_ip
	$JoinButton.hide()
	SendPlayerInformation($UserNameLabel/UserName.text, multiplayer.get_unique_id())


func _on_join_button_button_down():
	peer = ENetMultiplayerPeer.new()
	address = $IPAddressLabel/IPAddress.text
	print("Connecting to " + str(address))
	peer.create_client(address, int(PORT))
	peer.get_host().compress(ENetConnection.COMPRESS_RANGE_CODER) #Can use other compression schemes
	multiplayer.set_multiplayer_peer(peer)
	$StartGame.hide()
	$JoinButton.hide()
	$HostButton.hide()


func _on_start_game_button_down():
	if multiplayer.is_server():
		StartGame.rpc()


#@rpc("any_peer", "call_local")
#func instanceSpell(fireball_instance):
#	get_tree().root.add_child(fireball_instance)
