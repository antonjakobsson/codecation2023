class_name Gravity
extends Area2D

var lifespan = 0
const LIFESPAN_MAX = 5

# Animation
@onready var animationPlayer = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	collision_layer = 2
	collision_mask = 0
	$Sprite2D.modulate.a = 0.3


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	lifespan += delta
	rotation_degrees += 3
	if lifespan > LIFESPAN_MAX:
		queue_free()
		#animationPlayer.play("gravity_end")
		
#func _on_animation_player_animation_finished(anim_name):
	#if anim_name == "gravity_end":
	#	queue_free()
