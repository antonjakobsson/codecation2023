#Treat this as abstract class - not to be instantiated
extends CharacterBody2D

# Params
const SPEED = 300
const PUSHBACK = 500
var direction : Vector2
var hasHit = false


var damage = 10
var lifespan = 0


# Animation
@onready var animationPlayer = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	direction = Vector2(1,0).rotated(rotation).normalized()

# Called every frame. 'delta' is the elapsed time since the previous frame.1
func _physics_process(delta):
	lifespan += delta
	velocity = SPEED * direction
	#var collision = move_and_collide(velocity)
	if hasHit == false: # Only hit once
		var collision = move_and_slide()
		if collision:
			hasHit = true
			animationPlayer.play("fireball_hit")
			for i in get_slide_collision_count():
				#Apply force to all collisions
				var col = get_slide_collision(i).get_collider()
				if col is Player:
					col.push_back(direction*SPEED, PUSHBACK)
					


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "fireball_hit":
		queue_free()
