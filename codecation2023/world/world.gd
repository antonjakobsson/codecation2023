extends Node2D


#var tilemap_xmin = -75
#var tilemap_xmax = 105
#var tilemap_ymax = 96
#var tilemap_ymin = 126

var grass_width = 1280
var grass_height = 720

var growxdir = 1
var growydir = 0
var mapSizeX = grass_width / 16
var mapSizeY = grass_height / 16
var shrinkage = 0
var skip_frames = [1,2,3,4,5,6,7,8,9,10]
var shrinkage_index = 0
var frame_counter = 0

var x = 0
var y = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass


@rpc("any_peer", "call_local")
func increase_shrink():
	shrinkage += 1
	
@rpc("any_peer", "call_local")
func trigger_shrink():
	var coord = Vector2i(x, y)
	#var grasstiles = $GrassBackground.get("layer_0/tile_data")
	#$Lava.set("layer_0/tile_data", grasstiles)
	$Lava.set_cell(0,coord,0,Vector2i(0,0))

	x += growxdir
	y += growydir
	if x == mapSizeX-shrinkage and growxdir>0:
		growxdir = 0
		growydir = 1
	elif y == mapSizeY-shrinkage and growydir>0:
		growxdir = -1
		growydir = 0
	elif x == 0+shrinkage and growxdir<0:
		growydir = -1
		growxdir = 0
	elif y == 0+shrinkage and growydir<0:
		increase_shrink()
		growxdir = 1
		growydir = 0
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if multiplayer.is_server():
		if frame_counter % skip_frames[shrinkage_index] == 0:
			trigger_shrink.rpc()
		frame_counter += 1
		shrinkage_index = min(int(frame_counter/500), 9)
