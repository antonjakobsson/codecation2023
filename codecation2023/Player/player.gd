class_name Player
extends CharacterBody2D

const SPEED = 10000.0
const ACCELERATION = 10.0
const JUMP_VELOCITY = -400.0
const FRICTION = 2

const HP_MAX = 100
var hp = HP_MAX

# Spell Cooldowns
const FIREBALL_CD = 1.5
#const CAST_FIREBALL_DURATION = 1
var fireball_cooldown_left = 0 # Cooldown in seconds

const BLINK_CD = 20.0
var blink_cooldown_left = 0

const GLAIVE_CD = 3.0
var glaive_cooldown_left = 0

const INVISIBILITY_CD = 20
const INVISIBILITY_DURATION = 2.5
var invisibility_duration_left = 0
var invisibility_cooldown_left = 0

const GRAVITY_CD = 10
var gravity_cooldown_left = 0



# Player states
var player_on_lava = false
var player_invisible = false
var player_in_gravity = false
var gravity_position = Vector2(0,0)
var casting_left = 0



var playername = ""

@onready var animationPlayer = $AnimationPlayer
@onready var animationTree = $AnimationTree
@onready var animationState = animationTree.get("parameters/playback")
@onready var healthbar = $Healthbar/TextureProgressBar

var syncPos = Vector2(0, 0)
var syncRot = 0

@export var Spell = load("res://Spells/spell.tscn")
@export var Glaive = load("res://Spells/glaive.tscn")
@export var Gravity = load("res://Spells/gravity.tscn")
#@export var Spell :PackedScene

func _ready():
	#start animation
	animationTree.active = true
	$MultiplayerSynchronizer.set_multiplayer_authority(str(name).to_int())
	if $MultiplayerSynchronizer.get_multiplayer_authority() == multiplayer.get_unique_id():
		# Set each players camera view
		$Camera2D.make_current()
		$Camera2D/Spellbook.visible = true # Make UI visible only for your own view
		$Camera2D/FPS_counter.visible = true
	$BlinkSprite/Timer.connect("timeout", _on_blink_timer_timeout)
	$BlinkSprite.hide()

func accelerate(direction: Vector2, acceleration):
	velocity = velocity.move_toward(SPEED*direction, acceleration)


func apply_friction():
	velocity = velocity.move_toward(Vector2.ZERO, FRICTION)


func push_back(push_direction, push_force):
	# Called from spell hitting player instance
	velocity += velocity.move_toward(push_direction, push_force)

@rpc("any_peer", "call_local")
func _on_blink_timer_timeout():
	$BlinkSprite.hide()

@rpc("any_peer", "call_local")
func blink_animation():
	$BlinkSprite.show()
	$BlinkSprite/Timer.start(0.2)


func _physics_process(delta):
	if $MultiplayerSynchronizer.get_multiplayer_authority() == multiplayer.get_unique_id(): # Only control the right character
		casting_left = max(casting_left - delta, 0)
		var input_vector = Vector2.ZERO
		input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
		$SpellDirection.look_at(get_viewport().get_camera_2d().get_global_mouse_position())
		if input_vector != Vector2.ZERO:
			input_vector = input_vector.normalized()
			set_animation_travel(input_vector)
			#if casting_left == 0:
			accelerate(input_vector * delta, ACCELERATION)
		else:
			set_animation_idle()
			apply_friction()

		# Input key handling
		if Input.is_action_just_pressed("cast_spell"):
			if fireball_cooldown_left <= 0:
				set_animation_attack(input_vector)
				cast_fireball.rpc($SpellDirection/SpellSpawn.global_position, $SpellDirection.rotation_degrees)
				fireball_cooldown_left = FIREBALL_CD
				#casting_left = CAST_FIREBALL_DURATION

		if Input.is_action_just_pressed("movement_spell"):
			if blink_cooldown_left <= 0:
				blink_animation.rpc()
				
				position = get_viewport().get_camera_2d().get_global_mouse_position()
				velocity /= 2
				blink_cooldown_left = BLINK_CD

		if Input.is_action_just_pressed("cast_glaive"):
			if glaive_cooldown_left <= 0:
				cast_glaive.rpc($SpellDirection/SpellSpawn.global_position, $SpellDirection.rotation_degrees)
				glaive_cooldown_left = GLAIVE_CD

		if Input.is_action_just_pressed("cast_invisibility"):
			if invisibility_cooldown_left <= 0:
				player_invisible = true
				invisibility_duration_left = INVISIBILITY_DURATION
				invisibility_cooldown_left = INVISIBILITY_CD
				hide_player.rpc()

		if Input.is_action_just_pressed("cast_gravity"):
			if gravity_cooldown_left <= 0:
				cast_gravity.rpc(get_viewport().get_camera_2d().get_global_mouse_position())
				gravity_cooldown_left = GRAVITY_CD

		# Environmental effects
		if player_on_lava:
			take_damage.rpc(0.1) # Lava damage

		if player_in_gravity:
			var offset_pos = gravity_position - global_position
			accelerate(offset_pos, 11)
			
		if player_invisible:
			if invisibility_duration_left >= 0:
				invisibility_duration_left -= delta
			else:
				player_invisible = false
				show_player.rpc()
				
		# Reduce cooldown with delta time
		blink_cooldown_left -= delta
		fireball_cooldown_left -= delta
		glaive_cooldown_left -= delta
		invisibility_cooldown_left -= delta
		gravity_cooldown_left -= delta
		update_cooldownbar($Camera2D/Spellbook/Blink/CooldownBar/TextureProgressBar, blink_cooldown_left - delta, BLINK_CD)
		update_cooldownbar($Camera2D/Spellbook/Fireball/CooldownBar/TextureProgressBar, fireball_cooldown_left - delta, FIREBALL_CD)
		update_cooldownbar($Camera2D/Spellbook/Glaive/CooldownBar/TextureProgressBar, glaive_cooldown_left - delta, GLAIVE_CD)
		update_cooldownbar($Camera2D/Spellbook/Invisibility/CooldownBar/TextureProgressBar, invisibility_cooldown_left - delta, INVISIBILITY_CD)
		update_cooldownbar($Camera2D/Spellbook/Gravity/CooldownBar/TextureProgressBar, gravity_cooldown_left - delta, GRAVITY_CD)
		move_and_slide()

@rpc("any_peer", "call_local")
func hide_player():
	if $MultiplayerSynchronizer.get_multiplayer_authority() == multiplayer.get_unique_id():
		$Sprite2D.modulate.a = 0.5
	else:
		self.hide()
		
@rpc("any_peer", "call_local")
func show_player():
	self.show()
	$Sprite2D.modulate.a = 1.0
	
func set_animation_idle():
	animationState.travel("Idle")


func set_animation_travel(input_vector):
	animationTree.set("parameters/Idle/blend_position", input_vector)
	animationTree.set("parameters/Run/blend_position", input_vector)
	animationState.travel("Run")

func set_animation_attack(input_vector):
	print("Setting attack animation")
	animationTree.set("parameters/Attack/blend_position", input_vector)
	animationState.travel("Attack")

func update_cooldownbar(bar, value, cd):
	var cooldown_left = max(value, 0)
	bar.set_value(100 - (cooldown_left *(100/cd)))
	if bar.value < 100:
		bar.get_parent().get_parent().modulate.a = 0.5
	else:
		bar.get_parent().get_parent().modulate.a = 1.0
	


@rpc("any_peer", "call_local")
func cast_fireball(position, rotation):
	var spell_instance = Spell.instantiate()
	spell_instance.global_position = position
	spell_instance.rotation_degrees = rotation
	get_tree().root.add_child(spell_instance)
	
@rpc("any_peer", "call_local")
func cast_glaive(position, rotation):
	var glaive_instance = Glaive.instantiate()
	glaive_instance.global_position = position
	glaive_instance.rotation_degrees = rotation
	get_tree().root.add_child(glaive_instance)
	
@rpc("any_peer", "call_local")
func cast_gravity(mouse_position):
	var gravity_instance = Gravity.instantiate()
	gravity_instance.position = mouse_position
	get_tree().root.add_child(gravity_instance)

@rpc("any_peer", "call_local")
func take_damage(amount):
	hp -= amount
	healthbar.set_value_no_signal(hp)

func update_lava_state(state):
	player_on_lava = state

func pull_toward(state, gravity_pos):
	#Used to implement gravity
	gravity_position = gravity_pos
	player_in_gravity = state

func set_playername(text):
	$PlayerName.set_text(text)
	playername = text
